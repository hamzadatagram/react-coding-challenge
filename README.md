# Datagram Coding Challenge

> Welcome to the Datagram Coding Challenge!

## Overview

To complete this challenge, you will need to write a simple [React](https://facebook.github.io/react/) based web app, and provide us the source files to be built.

The purpose of this challenge is to assess your **skills and approach to composing a simple web app** given a set of pages and an API feed. We will also assess the **generated HTML, CSS, and JS** output.

This challenge is expected to take about 4-6 hours.

## The Challenge

It's pretty simple. Using the provided pages as a reference, you'll need to build a set of React components to render the app. You'll also need to request a JSON feed, filter that data, and use the relevant fields.

Although this is a basic exercise, we'll be looking for **simple, well-designed, performant, and tested code** in the submission.

Please include a `README` with setup instructions, and any tests or other documentation you created as part of your solution.

Also, add the following info to your `README`:

- How did you decide on the technical and architectural choices used as part of your solution?
- Are there any improvements you could make to your submission?
- What would you do differently if you were allocated more time?

## Details

You will need to build the following 3 pages with React:

- A "Home" page
- A "Users" page
- A "Posts" page
- A "Products" page
- A "Categories" page

The deployable solution should be built in a folder named **`dist`** with an entry point file of **`index.html`**.

Please create components for each part of the page (eg. header, content, footer, etc).
Assets are provided in the `assets` folder.

The pages should also be usable on mobile and tablet devices.

You can assume that you do not have to support legacy browsers without features such as `fetch` or `flexbox`.
In this case we prefer to use Axios to deal with API.

### "Home" Page

This page will show a welcome message and a menu at the left.

### Pages


For each page you will need to fetch this  [Online REST API](https://gorest.co.in/), then:

- Display the the list Users  with filters by ('created_at','name') and order by ('created_at','name')
- Display the the list Posts  with filters by ('created_at','title') and order by ('created_at')
- Detail page for user where we diplay the posts linked the it.
- Detail page for a post.

- Display the the list Products  with filters by ('price','status') and order by ('price','status')
- Display the the list Categories  with filters by ('name','description') and order by ('name')
- Detail page for product where we diplay the categories linked the it.


- "Loading" page.
- "Error" page.

## FAQ

### What language, framework, build tool... should I use?

You may use whatever you like as long as the solution is built using [React](https://facebook.github.io/react/) or an equivalent library.

We prefer it if you did not use any third party CSS frameworks.

We also prefer the use of minimal dependencies.

## Useful Links

- [Bitbucket](https://bitbucket.org/) - Source code hosting, with free private repositories for small teams.
- [Google Fonts - Raleway](https://fonts.google.com/?selection.family=Raleway)
- [React](https://facebook.github.io/react/)
- [MATERIAL-UI](https://material-ui.com/)

## Other Notes

Please send through any other code or projects that you're proud of and would like to share with us.

Any feedback on the coding challenge once you're done is also appreciated!